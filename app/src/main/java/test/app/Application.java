package test.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.data.querydsl.binding.QuerydslPredicateBuilder;
import org.springframework.format.support.DefaultFormattingConversionService;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("test")
@EntityScan("test.entity")
@EnableJpaRepositories(basePackages = "test.service.repository")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public QuerydslPredicateBuilder querydslPredicateBuilder() {
        return new QuerydslPredicateBuilder(
                new DefaultFormattingConversionService(), SimpleEntityPathResolver.INSTANCE
        );
    }
}
