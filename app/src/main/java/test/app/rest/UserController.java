package test.app.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import test.command.GetUsersParamsCommand;
import test.command.GetUsersTsvCommand;
import test.domain.Response;
import test.domain.UserData;
import test.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {
    private final UserService userService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Response<List<UserData>> getUsers() {
        return new Response<>(userService.getAllUsers());
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Response<Page<UserData>> getAllUsersByParams(
            GetUsersParamsCommand getUsersParamsCommand,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return new Response<>(userService.getAllUsersByParams(getUsersParamsCommand, pageable));
    }

    @RequestMapping(value = "/tsv", method = RequestMethod.GET)
    public Response<Page<UserData>> getAllUsersTsv(
            GetUsersTsvCommand getUsersTsvCommand,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return new Response<>(userService.getAllUsersTsv(getUsersTsvCommand, pageable));
    }
}
