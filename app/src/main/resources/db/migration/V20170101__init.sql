CREATE TABLE users (
  id          BIGINT PRIMARY KEY,
  created     TIMESTAMPTZ,
  username    TEXT,
  firstname   TEXT,
  lastname    TEXT,
  email       TEXT,
  nationality TEXT,
  password    TEXT,
  is_deleted  BOOLEAN,
  is_blocked  BOOLEAN,
  languages   TEXT [],
  tsv         TSVECTOR
);

CREATE TABLE photo (
  id      BIGINT PRIMARY KEY,
  large   TEXT,
  medium  TEXT,
  small   TEXT,
  email   TEXT,
  user_id BIGINT
);

CREATE TABLE address (
  id      BIGINT PRIMARY KEY,
  city    TEXT,
  street  TEXT,
  zip     TEXT,
  user_id BIGINT
);

CREATE TRIGGER trg_users_tsvectorupdate
BEFORE INSERT OR UPDATE
  ON users
FOR EACH ROW EXECUTE PROCEDURE
  tsvector_update_trigger(tsv, 'pg_catalog.english', username, firstname, lastname, email);