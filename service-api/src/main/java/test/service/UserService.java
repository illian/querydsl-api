package test.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import test.command.GetUsersParamsCommand;
import test.command.GetUsersTsvCommand;
import test.domain.UserData;

import java.util.List;


public interface UserService {
    List<UserData> getAllUsers();

    Page<UserData> getAllUsersByParams(GetUsersParamsCommand command, Pageable pageable);

    Page<UserData> getAllUsersTsv(GetUsersTsvCommand command, Pageable pageable);
}
