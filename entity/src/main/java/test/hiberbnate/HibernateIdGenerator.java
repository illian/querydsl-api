package test.hiberbnate;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;


public class HibernateIdGenerator implements IdentifierGenerator {
    public static final String GENERATOR_CLASS = "test.hiberbnate.HibernateIdGenerator";
    public static final long OUR_EPOCH = 1451606400L * 1_000;
    public static final int SHARD_ID_LENGTH = 10;
    public static final int INCREMENT_ID_LENGTH = 10;
    public static final int SUM_LENGTH = SHARD_ID_LENGTH + INCREMENT_ID_LENGTH;

    private final AtomicLong atomicCounter = new AtomicLong(0);

    @Override
    public Serializable generate(SessionImplementor sessionImplementor, Object o) throws HibernateException {
        long id = (System.currentTimeMillis() - OUR_EPOCH) << SUM_LENGTH;
        long counter = atomicCounter.getAndIncrement() % 1024;
        id |= 1 << INCREMENT_ID_LENGTH;
        id |= counter;

        return id;
    }
}
