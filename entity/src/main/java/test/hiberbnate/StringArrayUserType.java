package test.hiberbnate;

public class StringArrayUserType extends ArrayUserType<String> {
    public StringArrayUserType() {
        super(String[].class);
    }
}
