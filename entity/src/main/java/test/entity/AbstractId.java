package test.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import test.hiberbnate.HibernateIdGenerator;

import javax.persistence.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
@MappedSuperclass
public abstract class AbstractId {
    public static final String DT_GENERATOR = "dt-generator";

    @Id
    @GenericGenerator(name = DT_GENERATOR, strategy = HibernateIdGenerator.GENERATOR_CLASS)
    @GeneratedValue(generator = DT_GENERATOR, strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
}
