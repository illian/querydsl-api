package test.command;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class GetUsersParamsCommand {
    private List<Date> createdBetween;

    private String username;

    private String firstname;

    private String lastname;

    private String email;

    private String containsLang;

    private String city;
}
