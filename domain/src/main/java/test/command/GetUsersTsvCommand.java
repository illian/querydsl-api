package test.command;

import lombok.Data;

@Data
public class GetUsersTsvCommand {
    private String query;
}
