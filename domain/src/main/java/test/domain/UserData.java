package test.domain;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserData {
    private Date created;

    private String username;

    private String firstname;

    private String lastname;

    private String email;

    private String nationality;

    private String password;

    private Boolean isDeleted;

    private Boolean isBlocked;

    private List<String> languages;

    private AddressData addressData;
}
