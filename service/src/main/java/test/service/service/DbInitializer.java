package test.service.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.separator.DefaultRecordSeparatorPolicy;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import test.entity.AddressEntity;
import test.entity.Photo;
import test.entity.UserEntity;
import test.service.repository.AddressRepository;
import test.service.repository.PhotoRepository;
import test.service.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.capitalize;


@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DbInitializer {
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final PhotoRepository photoRepository;

    @PostConstruct
    @Transactional
    public void init() throws Exception {
        long count = userRepository.count();
        if (count == 0) {
            addressRepository.deleteAll();
            photoRepository.deleteAll();
            List<UserEntity> userEntities = readUsers(new ClassPathResource("randomuser.me.csv"));
            for (UserEntity userEntity : userEntities) {
                AddressEntity address = userEntity.getAddress();
                Photo photo = userEntity.getPhoto();
                userEntity.setAddress(null);
                userEntity.setPhoto(null);
                userRepository.save(userEntity);
                addressRepository.save(address);
                photoRepository.save(photo);
            }
        }
    }

    private List<UserEntity> readUsers(Resource resource) throws Exception {
        List<String> languages = new ArrayList<>();
        languages.add("english");
        languages.add("german");
        languages.add("french");
        languages.add("russian");
        languages.add("chinese");
        languages.add("japanese");
        languages.add("swedish");

        Random random = new Random(1L);
        Scanner scanner = new Scanner(resource.getInputStream());
        String line = scanner.nextLine();
        scanner.close();

        FlatFileItemReader<UserEntity> reader = new FlatFileItemReader<>();
        reader.setResource(resource);

        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(line.split(","));
        tokenizer.setStrict(false);

        DefaultLineMapper<UserEntity> lineMapper = new DefaultLineMapper<>();
        lineMapper.setFieldSetMapper(fields -> {
            UserEntity userEntity = new UserEntity();
            userEntity.setCreated(new Date());

            userEntity.setEmail(fields.readString("email"));
            userEntity.setFirstname(capitalize(fields.readString("first")));
            userEntity.setLastname(capitalize(fields.readString("last")));
            userEntity.setNationality(fields.readString("nationality"));

            String city = Arrays.stream(fields.readString("city").split(" "))//
                    .map(StringUtils::capitalize)//
                    .collect(Collectors.joining(" "));
            String street = Arrays.stream(fields.readString("street").split(" "))//
                    .map(StringUtils::capitalize)//
                    .collect(Collectors.joining(" "));

            try {
                userEntity.setAddress(new AddressEntity(city, street, fields.readString("zip"), userEntity));
            } catch (IllegalArgumentException e) {
                userEntity.setAddress(new AddressEntity(city, street, fields.readString("postcode"), userEntity));
            }

            userEntity.setPhoto(new Photo(fields.readString("large"), fields.readString("medium"),
                    fields.readString("thumbnail"), userEntity));

            userEntity.setUsername(fields.readString("username"));
            userEntity.setPassword(fields.readString("password"));

            if (random.nextInt(20) == 0) {
                userEntity.setIsDeleted(true);
            } else {
                userEntity.setIsDeleted(false);
            }

            if (random.nextInt(20) == 0) {
                userEntity.setIsBlocked(true);
            } else {
                userEntity.setIsBlocked(false);
            }

            userEntity.setLanguages(getRandomLanguagesArray(languages, random));

            return userEntity;
        });

        lineMapper.setLineTokenizer(tokenizer);

        reader.setLineMapper(lineMapper);
        reader.setRecordSeparatorPolicy(new DefaultRecordSeparatorPolicy());
        reader.setLinesToSkip(1);
        reader.open(new ExecutionContext());

        List<UserEntity> userEntities = new ArrayList<>();
        UserEntity userEntity;

        do {
            userEntity = reader.read();

            if (userEntity != null) {
                userEntities.add(userEntity);
            }

        } while (userEntity != null);

        return userEntities;
    }

    private String[] getRandomLanguagesArray(List<String> languageList, Random random) {
        int size = random.nextInt(2) + 1;
        String[] result = new String[size];
        for (int i = 0; i < size; i++) {
            result[i] = languageList.get(random.nextInt(languageList.size() - 1));
        }

        return result;
    }
}
