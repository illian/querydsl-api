package test.service.service;

import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.QuerydslPredicateBuilder;
import org.springframework.data.util.ClassTypeInformation;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import test.command.GetUsersParamsCommand;
import test.command.GetUsersTsvCommand;
import test.domain.AddressData;
import test.domain.UserData;
import test.entity.QUserEntity;
import test.entity.UserEntity;
import test.service.UserService;
import test.service.db.PredicateBuilder;
import test.service.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {
    private static final ClassTypeInformation<UserEntity> USER_TYPE = ClassTypeInformation.from(UserEntity.class);
    private final UserRepository userRepository;
    private final QuerydslPredicateBuilder builder;

    @Override
    public List<UserData> getAllUsers() {
        return userRepository.findAll().stream()
                .map(UserServiceImpl::convert)
                .collect(Collectors.toList());
    }

    @Override
    public Page<UserData> getAllUsersByParams(GetUsersParamsCommand command, Pageable pageable) {
        return userRepository.findAll(getPredicate(command, true), pageable)
                .map(UserServiceImpl::convert);
    }

    @Override
    public Page<UserData> getAllUsersTsv(GetUsersTsvCommand command, Pageable pageable) {
        return userRepository.findAll(getPredicate(command, true), pageable)
                .map(UserServiceImpl::convert);
    }

    private Predicate getPredicate(Object command, boolean excludeDeleted) {
        MultiValueMap<String, String> values = PredicateBuilder.getMap(command);
        QuerydslBindings querydslBindings = new QuerydslBindings();
        userRepository.customize(querydslBindings, QUserEntity.userEntity);
        Predicate predicate = builder.getPredicate(USER_TYPE, values, querydslBindings);
        if (excludeDeleted) {
            predicate = userRepository.isNotDeleted(predicate);
        }

        return predicate;
    }

    private static UserData convert(UserEntity userEntity) {
        UserData userData = new UserData();

        userData.setCreated(userEntity.getCreated());
        userData.setEmail(userEntity.getEmail());
        userData.setFirstname(userEntity.getFirstname());
        userData.setLastname(userEntity.getLastname());
        userData.setIsBlocked(userEntity.getIsBlocked());
        userData.setIsDeleted(userEntity.getIsDeleted());
        userData.setAddressData(
                new AddressData(userEntity.getAddress().getCity(), userEntity.getAddress().getStreet()));

        return userData;
    }
}
