package test.service.repository;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import test.entity.QUserEntity;
import test.entity.UserEntity;
import test.service.db.PostgresCustomDialect;


public interface UserRepository extends JpaRepository<UserEntity, Long>, QueryDslPredicateExecutor<UserEntity>,
        QuerydslBinderCustomizer<QUserEntity> {

    @Override
    default void customize(QuerydslBindings bindings, QUserEntity root) {
        bindings.bind(root.created).first(TemporalExpression::after);
        bindings.bind(root.address.city).as("city").first(StringExpression::startsWith);
        bindings.bind(String.class).first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
        bindings.bind(root.tsv).as("query").first((path, query) ->
                Expressions.booleanTemplate(String.format(PostgresCustomDialect.FULL_TEXT_SEARCH, path, query)));
        bindings.bind(root.languages).as("containsLang").first((path, strings) ->
                Expressions.booleanTemplate(String.format(PostgresCustomDialect.ARRAY_CONTAINS, path), strings[0]));
        bindings.excluding(root.password);
    }

    default BooleanExpression isNotDeleted(Predicate predicate) {
        return QUserEntity.userEntity.isDeleted.eq(false).and(predicate);
    }
}
