package test.service.db;

import org.hibernate.dialect.PostgreSQL82Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.dialect.function.VarArgsSQLFunction;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.TextType;


public class PostgresCustomDialect extends PostgreSQL82Dialect {
    public static final String ARRAY_CONTAINS = "{0} = any_arr(%s)";
    public static final String FULL_TEXT_SEARCH = "true = fts(%s, '%s')";

    public PostgresCustomDialect() {
        registerFunction("any_arr", new StandardSQLFunction("any", TextType.INSTANCE));
        registerFunction("fts", new PostgreSQLFullTextSearchFunction());
        registerFunction("concat_ws", new VarArgsSQLFunction(StandardBasicTypes.STRING, "concat_ws(' ',", ",", ")"));
    }
}
