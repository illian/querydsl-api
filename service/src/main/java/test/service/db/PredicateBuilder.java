package test.service.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

@Slf4j
public class PredicateBuilder {
    public static MultiValueMap<String, String> getMap(Object object) {
        MultiValueMap<String, String> result = new LinkedMultiValueMap<>();

        Class<?> clazz = object.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(object);
                if (value instanceof Object[]) {
                    Object[] arr = (Object[]) value;
                    for (Object item : arr) {
                        result.add(field.getName(), getStringValue(item));
                    }
                } else if (value instanceof List) {
                    List list = (List) value;
                    for (Object item : list) {
                        result.add(field.getName(), getStringValue(item));
                    }
                } else if (value instanceof Set) {
                    Set set = (Set) value;
                    for (Object item : set) {
                        result.add(field.getName(), getStringValue(item));
                    }
                } else {
                    result.add(field.getName(), getStringValue(value));
                }
            } catch (IllegalAccessException e) {
                log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    private static String getStringValue(Object value) {
        return value == null ? null : value.toString();
    }
}
